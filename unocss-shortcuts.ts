// @unocss-include

const dynamicShortcuts = [
  ['background', 'white', 'neutral-900'],
  ['foreground', 'neutral-900', 'zinc-50'],
  ['primary', 'zinc-900', 'zinc-50'],
  ['primary-foreground', 'zinc-50', 'zinc-900'],
  ['secondary', 'zinc-100', 'zinc-800'],
  ['secondary-foreground', 'zinc-900', 'zinc-50'],
  ['muted', 'zinc-100', 'zinc-800'],
  ['muted-foreground', 'zinc-500', 'zinc-500'],
  ['accent', 'gray-200', 'gray-700'],
  ['accent-foreground', 'zinc-900', 'zinc-50'],
  ['destructive', 'red-500', 'red-900'],
  ['destructive-foreground', 'zinc-50', 'zinc-50'],
  ['border', 'zinc-200', 'zinc-800'],
  ['input', 'zinc-200', 'zinc-800'],
  ['ring', 'zinc-300', 'zinc-900'],
  ['active', 'blue-700', 'blue-500'],
].map(colors => createColorShortcuts(...colors))

const FLOATING_MENU = clsx(
  'z-10 box-border rounded-lg border border-border bg-background shadow-lg'
  )

const FLOATING_MENU_ITEM = clsx(
  'box-border cursor-default select-none whitespace-nowrap outline-none aria-selected:bg-secondary'
)

const BUTTON_BASE = clsx(
  'inline-flex items-center justify-center whitespace-nowrap rounded-md text-sm font-medium ring-offset-background transition-colors focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:pointer-events-none disabled:opacity-50'
)

const BUTTON_VARIANT_PRIMARY = 'bg-primary text-primary-foreground hover:bg-primary/90'
const BUTTON_VARIANT_SECONDARY = 'bg-secondary text-secondary-foreground hover:bg-secondary/80'

const TOGGLE_BUTTON = clsx(
  'outline-unset focus-visible:outline-unset inline-flex items-center justify-center px-2 py-1.5 font-medium transition focus-visible:ring-2 focus-visible:ring-ring disabled:pointer-events-none',
  'disabled:opacity-50 hover:disabled:opacity-50',
  'dark:text-white hover:bg-secondary data-[state=on]:bg-blue-300/10 dark:data-[state=on]:bg-blue-800/20 data-[state=on]:text-blue-500!'
)

const BUTTON_SIZE_DEFAULT = 'h-10 px-4 py-2'
const BUTTON_SIZE_SM = 'h-9 px-3'
const BUTTON_SIZE_LG = 'h-1 px-8'
const BUTTON_SIZE_ICON = 'h-10 w-10'

const INPUT = clsx(
  'flex h-8 rounded-md w-full border box-border bg-background px-2 py-1 text-sm placeholder:text-muted-foreground transition',
  // border
  'border-border border-solid border',
  // ring
  'ring-0 ring-transparent focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-0',
  // outline
  'outline-none focus-visible:outline-none',
  // file
  'file:border-0 file:bg-transparent file:text-sm file:font-medium',
  // disabled
  'disabled:cursor-not-allowed disabled:opacity-50'
)

const staticShortcuts = {
  // The outermost container of the editor. It limits the height of the editor.
  EDITOR_VIEWPORT: clsx(
    'h-full w-full min-h-32 overflow-y-auto overflow-x-hidden'
  ),

  // Use this class if you have floating menus. We want to scroll menus along with the document.
  EDITOR_DOCUMENT: clsx('relative flex min-h-full w-full flex-col'),

  // Use this class for the contenteditable element.
  EDITOR_CONTENT: clsx(
    'relative box-border min-h-full flex-1 overflow-auto outline-none outline-0',
    '[&_span[data-mention="user"]]:color-blue-500',
    '[&_span[data-mention="tag"]]:color-violet-500',
    '[&_pre]:bg-zinc-100 dark:[&_pre]:color-white dark:[&_pre]:bg-zinc-800',

    // NOTE Enforce caret color for gradient text with `text-transparent bg-clip-text`
    'caret-black dark:caret-white'
  ),

  FRONTMATTER: clsx(
    'mb-4 bg-zinc-100 dark:bg-zinc-800 rounded-md [&_pre]:bg-transparent! [&_pre]:m-0 [&_pre]:pt-0 [&_pre]:pb-2 [&_pre]:px-3 [&_pre]:leading-5'
  ),
  FRONTMATTER_TOGGLE_BUTTON: clsx(
    'font-mono text-xs text-left select-none w-full py-2 px-3'
  ),
  FRONTMATTER_DOC: clsx(
    '[&_pre]:pt-2! bg-zinc-200 dark:bg-zinc-700 rounded-b-md'
  ),
  FRONTMATTER_ERROR: clsx(
    'text-sm bg-red-400 dark:bg-red-900 p3'
  ),

  INLINE_MENU_MAIN: clsx(
    FLOATING_MENU,

    'relative flex items-stretch min-w-[120px] h-8 gap-0 p-0 overflow-auto whitespace-nowrap rounded-md'
  ),

  INLINE_MENU_LINK: clsx(
    FLOATING_MENU,
    'relative flex flex-col w-xs rounded-lg p-2 gap-y-2 items-stretch'
  ),

  INLINE_MENU_LINK_INPUT: clsx(INPUT),

  INLINE_MENU_LINK_REMOVE_BUTTON: clsx(
    BUTTON_BASE,
    BUTTON_VARIANT_PRIMARY,
    BUTTON_SIZE_SM
  ),

  AUTOCOMPLETE_MENU: clsx(
    'relative block max-h-[400px] min-w-[120px] select-none overflow-auto whitespace-nowrap p-1',
    FLOATING_MENU
  ),

  AUTOCOMPLETE_MENU_ITEM: clsx(
    'relative block min-w-[120px] scroll-my-1 rounded px-3 py-1.5',
    FLOATING_MENU_ITEM
  ),

  LANGUAGE_WRAPPER: clsx(
    'relative left-2 top-3 h-0 select-none overflow-visible'
  ),

  LANGUAGE_SELECT: clsx(
    'outline-unset focus:outline-unset relative box-border w-auto cursor-pointer select-none appearance-none rounded border-none bg-transparent px-2 py-1 text-xs transition dark:color-white',

    // Only visible when hovering the code block
    'opacity-0 hover:opacity-80 [div[data-node-view-root]:hover_&]:opacity-50 [div[data-node-view-root]:hover_&]:hover:opacity-80'
  ),

  TOOLBAR: clsx(
    'z-2 sticky top-0 box-border flex flex-wrap gap-1 p-2 items-center bg-background',
    'border-border border-solid border-l-0 border-r-0 border-t-0 border-b'
  ),

  TOGGLE_BUTTON,

  IMAGE_UPLOAD_CARD: clsx(
    'flex flex-col gap-y-4 p-6 text-sm w-sm',
    FLOATING_MENU
  ),

  IMAGE_UPLOAD_INPUT: clsx(INPUT),

  IMAGE_UPLOAD_BUTTON: clsx(
    BUTTON_BASE,
    BUTTON_VARIANT_PRIMARY,
    BUTTON_SIZE_DEFAULT,
    'w-full'
  ),

  IMAGE_RESIZEALE: clsx('relative block max-h-[600px] max-w-full'),

  IMAGE_RESIZEALE_IMAGE: clsx('h-full w-full object-contain'),

  IMAGE_RESIZEALE_HANDLE: clsx(
    'absolute bottom-0 right-0 rounded mb-1.5 mr-1.5 p-0.5 transition bg-gray-900/30 active:bg-gray-800/60 text-white/50 active:text-white/80 active:translate-x-0.5 active:translate-y-0.5',
    // Only visible when hovering the image block
    'opacity-0 hover:opacity-100 [prosekit-resizable:hover_&]:opacity-100 [prosekit-resizable[data-resizing]_&]:opacity-100'
  ),

  DROP_CURSOR: clsx('transition-all bg-blue-500'),

  BLOCK_HANDLE: clsx(
    'cursor-grab flex items-center box-border justify-center h-[1.5em] w-[1.2em] hover:bg-secondary rounded text-muted-foreground/50 transition-colors'
  ),

  BLOCK_HANDLE_MENU_ITEM: clsx(
    'outline-unset focus-visible:outline-unset inline-flex items-center gap-2.5 w-full rounded px-2 py-0.5 p-2 text-sm font-medium transition focus-visible:ring-2 focus-visible:ring-ring disabled:pointer-events-none',
    'disabled:opacity-50 hover:disabled:opacity-50',
    'bg-transparent hover:bg-secondary data-[state=on]:text-active'
  ),

  SECTION_MENU: clsx(
    FLOATING_MENU,

    'relative block space-x-0 p-0 overflow-auto text-xs whitespace-nowrap rounded-md'
  ),

  SECTION_MENU_BUTTON: clsx(
    TOGGLE_BUTTON,
    'py-3!'
  ),

  ICON_ITALIC: clsx('i-lucide-italic h-4 w-4'),
  ICON_BOLD: clsx('i-lucide-bold h-4 w-4'),
  ICON_UNDERLINE: clsx('i-lucide-underline h-4 w-4'),
  ICON_STRIKE: clsx('i-lucide-strikethrough h-4 w-4'),
  ICON_CODE: clsx('i-lucide-code h-4 w-4'),
  ICON_H1: clsx('i-lucide-heading-1 h-4 w-4'),
  ICON_H2: clsx('i-lucide-heading-2 h-4 w-4'),
  ICON_H3: clsx('i-lucide-heading-3 h-4 w-4'),
  ICON_H4: clsx('i-lucide-heading-4 h-4 w-4'),
  ICON_H5: clsx('i-lucide-heading-5 h-4 w-4'),
  ICON_H6: clsx('i-lucide-heading-6 h-4 w-4'),
  ICON_UNDO: clsx('i-lucide-undo-2 h-4 w-4'),
  ICON_REDO: clsx('i-lucide-redo-2 h-4 w-4'),
  ICON_IMAGE: clsx('i-lucide-image h-4 w-4'),
  ICON_LINK: clsx('i-lucide-link h-4 w-4'),
  ICON_LIST_BULLET: clsx('i-lucide-list h-4 w-4'),
  ICON_LIST_ORDERED: clsx('i-lucide-list-ordered h-4 w-4'),
  ICON_LIST_TASK: clsx('i-lucide-list-checks h-4 w-4'),
  ICON_LIST_TOGGLE: clsx('i-lucide-list-collapse h-4 w-4'),
  ICON_CODE_BLOCK: clsx('i-lucide-square-code h-4 w-4'),
  ICON_CORNER_HANDLE: clsx('i-lucide-arrow-down-right h-4 w-4'),
  ICON_DRAG_HANDLE: clsx('i-lucide-grip-vertical h-4 w-4'),
  ICON_DELETE: clsx('i-lucide-trash-2 h-4 w-4'),
  ICON_COPY: clsx('i-lucide-copy h-4 w-4'),
  ICON_CUSTOM_HTML: clsx('i-lucide-code h-4 w-4'),
}

function replaceDynamicShortcuts(text) {
  if (typeof text !== 'string') {
    throw new TypeError('Expects a string')
  }

  if (text.includes(' ')) {
    throw new Error(`Expects a single class name. Received "${text}"`)
  }

  for (const [pattern, replacer] of dynamicShortcuts) {
    pattern.lastIndex = 0
    const match = text.match(pattern)
    pattern.lastIndex = 0
    if (match) {
      return replacer(match)
    }
  }
  return text
}

function clsx(...parts) {
  return parts
    .map(p => p || '')
    .map(p => p.split(' '))
    .flat()
    .filter(p => p)
    .map(replaceDynamicShortcuts)
    .join(' ')
}

/**
 * Replace color aliases.
 */
function createColorShortcuts(name, color, darkColor) {
  const pattern = new RegExp(
    `^(.*(?:text|bg|border|ring|ring-offset))-(${name})(\\/.*)?$`,
    'g'
  )

  return [
    pattern,
    (match) => {
      const input = String(match[0])

      if (input.includes(' ')) {
        throw new Error(`Expects a single class name. Received "${input}"`)
      }

      return String(match[0]).replace(
        pattern,
        `$1-${color}$3 dark:$1-${darkColor}$3`
      )
    },
  ]
}

/**
 * Replace CSS class names from shortcuts with the actual tailwindcss/unocss
 * class names.
 *
 * @param {string} code - The code to be processed.
 * @returns {string} - The processed code.
 */
export function replaceShortcuts(code) {
  // Sort by length, so that longer shortcuts are replaced first
  const shortcutNames = Object.keys(staticShortcuts).sort(
    (a, b) => b.length - a.length
  )

  return (
    code
      // Replace " with ', because some class names contain "
      .replace(
        new RegExp(`\"(${shortcutNames.join('|')})\"`, 'g'),
        match => '\'' + staticShortcuts[match.slice(1, -1)] + '\''
      )
      .replace(
        new RegExp(`\\b(${shortcutNames.join('|')})\\b`, 'g'),
        match => staticShortcuts[match]
      )
  )
}

export const shortcuts = [staticShortcuts, ...dynamicShortcuts]
