import { resolve } from 'node:path'
import { transformerDirectives } from 'unocss'
// import { execSync } from 'child_process'

import config from './content/settings.json'
import { shortcuts } from './unocss-shortcuts'
// import { shortcuts } from 'mdc-editor/unocss-shortcuts'


const publicRuntimeConfig = {
  site_url: process.env.site_url || '',
  twitter_user: process.env.twitter_user || '',
  social_networks_hashtags: process.env.social_networks_hashtags || '',
  git_commit: '', // execSync('git log --pretty=format:"%h" -n 1').toString().trim(),
  ...config
}

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  modules: [
    '@nuxt/devtools',
    // https://content.nuxt.com/
    '@nuxt/content', // enabled by default via docus
    // https://image.nuxt.com/
    '@nuxt/image',
    // https://github.com/kevinmarrec/nuxt-pwa-module
    // '@kevinmarrec/nuxt-pwa',
    // https://uno.antfu.me/
    '@unocss/nuxt',
    '@unpress/nuxt-module'
    // '../../unpress/packages/nuxt-module/src/module'
  ],

  unpress: {
    repo: 'https://git.duniter.org/websites/axiom-team-fr'
  },

  devtools: {
    enabled: true
  },

  // https://elements.nuxt.space
  // https://docus.dev/api/components
  extends: [
    '@nuxt-themes/typography'
    // 'nuxt-umami'
  ],

  content: {
    documentDriven: false
  },

  // Config
  publicRuntimeConfig,

  /**
   * Global page headers (https://v3.nuxtjs.org/api/configuration/nuxt.config#head)
   */
  app: {
    head: {
      title: config.site_title || process.env.npm_package_name || '',
      meta: [
        {
          name: 'description',
          content:
            config.site_description || process.env.npm_package_description
        }
      ],
      link: [
        {
          rel: 'stylesheet',
          href: 'https://fonts.bunny.net/css?family=Russo-One&display=swap"'
        },
        { rel: 'icon', type: 'image/x-icon', href: config.site_logo }
      ],
      bodyAttrs: {
        // For tailwindcss in dev mode
        class: process.env.NODE_ENV === 'development' ? 'debug-screens' : ''
      }
    }
  },

  unocss: {
    shortcuts,
    preflight: true,
    typography: true,
    icons: true,
    transformers: [transformerDirectives({ enforce: 'pre' })], // use enforce: 'pre' to correctly handle @apply. See https://github.com/unocss/unocss/issues/809#issuecomment-1118632177
    theme: {
      colors: {
        red: {
          50: '#FFF5F5',
          100: '#FFE5E6',
          200: '#FFC7C9',
          300: '#FCA1A4',
          400: '#F66F74',
          500: '#D91419',
          600: '#C50C13',
          700: '#AD050B',
          800: '#8E0105',
          900: '#6B0004'
        },
        primary: '#D91419'
      }
    },
    variants: [
      // hover:
      (matcher) => {
        if (!matcher.startsWith('sepia:')) { return matcher }
        return {
          // slice `hover:` prefix and passed to the next variants and rules
          matcher: matcher.slice(6),
          selector: (s) => `.sepia ${s}`
        }
      }
    ]
  },

  colorMode: {
    preference: 'dark', // default value of $colorMode.preference
    fallback: 'dark' // fallback value if not system preference found
  },

  images: {
    provider: 'netlify'
  },

  // https://pwa.nuxtjs.org/manifest
  pwa: {
    meta: {
      name: config.site_title,
      author: undefined,
      lang: 'fr',
      theme_color: '#D91419',
      description: config.site_description,
      ogHost: config.site_url,
      twitterCard: 'summary_large_image'
    },
    manifest: {
      name: config.site_title,
      short_name: config.site_url,
      lang: 'fr'
    },
    icon: {
      fileName: config.site_logo
    }
  },

  build: {
    transpile: ['@yeger/vue-masonry-wall', 'swiper']
  },

  // alias: {
  //   // 'nuxt-cms': r('../../nuxt-cms2/packages/nuxt-cms/src/module'),
  //   // 'nuxt-cms-server': r('../../nuxt-cms2/packages/nuxt-cms-server/src/module'),
  //   // 'mdc-editor': r('../../nuxt-cms2/packages/mdc-editor/src/module'),
  //   // 'mdc-editor/unocss-shortcuts': r('../../nuxt-cms2/packages/mdc-editor/src/module/runtime/static/unocss-shortcuts.ts'),
  //   '@unpress/nuxt-module': r('../../unpress/packages/nuxt-module/src/module'),
  //   '@unpress/mdc-editor': r('../../unpress/packages/mdc-editor/src/module'),
  // },

  // vite: {
  //   build: {
  //     rollupOptions: {
  //       input: {
  //         '@unpress/nuxt-module': r('../../unpress/packages/nuxt-module/src/module'),
  //         '@unpress/mdc-editor': r('../../unpress/packages/mdc-editor/src/module'),
  //         // 'mdc-editor': r('../../nuxt-cms2/packages/mdc-editor/src/module'),
  //         // 'mdc-editor/unocss-shortcuts': r('../../nuxt-cms2/packages/mdc-editor/src/module/runtime/static/unocss-shortcuts.ts')
  //       }
  //     }
  //   }
  // },

  // https://github.com/unocss/unocss/issues/2113#issuecomment-1423045289
  sourcemap: {
    server: true,
    client: false
  },

  compatibilityDate: '2024-09-11'
})

function r(p: string) {
  return resolve(__dirname, p)
}