---
title: Accueil
description: Bienvenue sur le site d'axiom-team.
---

::block-hero
---
cta:
  - Adhérer
  - /adherer
image: uploads/hero-home.svg
secondary:
  - Blog
  - /blog
---
#title
Au service de\
la monnaie libre

#description
Nous croyons en la monnaie libre, nous soutenons activement son développement et le maintien de son écosystème technique, nous facilitons sa promotion et nous accompagnons des groupes locaux qui le souhaitent.

#support
:hero-home
::


::callout{icon="heroicons-information-circle-20-solid" type="danger"}
#summary
Un financement participatif pour terminer la v2 des logiciels est en cours !

#content
<img src="/uploads/blog/crowdfunding-2024.png" style="width: 400px; margin: auto;" alt="nouvelle maison G1">

Voir [la campagne](https://www.helloasso.com/associations/axiom-team/collectes/finalisation-de-cesium-v2-et-duniter-v2) sur HelloAsso.
::


::newsletter{#c99fb6a6-d35c-4b12-a328-34b04cee5ce6}
### Abonnez-vous à notre newsletter

Recevez l'actualité de notre association. (\~ 1 mail / mois)
::

Vos adhésions constituent la seule ressource régulière pour le fonctionnement de l’association elle-même, quelques aspects techniques lorsqu’ils sont nécessaires, et surtout financer les rassemblements humains, comme les « axiom boat », les hackathons, et autres événements qu’elle organise.
[Adhérer](https://www.helloasso.com/associations/axiom-team/adhesions/adhesion-2024-axiom-team)

[Statuts de l'association](https://cloud.axiom-team.fr/s/Dwzpw7PAofNroof) : ce sont les statuts originaux, en cours de ré-écriture avec l'aide de Finacoop, afin de répondre aux pré-requis de l'agrément ESUS que l'association souhaite obtenir.

## Mais c'est quoi la monnaie libre ?

A priori, si vous êtes ici c'est que vous connaissez la monnaie libre. Mais si vous souhaitez en apprendre plus, nous recommandons ce point de départ : le site **[monnaie-libre.fr](https://monnaie-libre.fr/)**

Vous y trouverez tous les liens sur les forums et les sites ressources.

## L'association Axiom-Team remplit trois missions :

### Développement et financement

**Soutenir les développeurs de l'écosystème Duniter** en mettant à disposition des outils d'organisation et des financements. Pour cela, nous avons [deux cagnottes](https://axiom-team.fr/financement) permanentes destinées à accueillir les dons en euro et ğ1 et constituer un fond de redistribution. Cette collecte de dons est la principale ressource de ce fond.
Le seul autre moyen de l'alimenter est le recours à des subventions pures, comme cela a été le cas avec l'ADEME une première fois. Cette ressource est aléatoire et peut requérir des mois ou des années de patience. De plus elles sont soumises, la plupart du temps, à une proportion d'auto-financement (constitué avec vos dons).

### Support(s) de comm

**Aider les utilisateurs à promouvoir la monnaie libre** en fournissant des outils pour organiser des événements locaux. Nous proposons des créations graphiques (modèles de flyers, d'affiches, de stickers), nous partageons les expériences et documents, nous rendons d'autres services selon les besoins des groupes locaux.
[Médiathèque](https://axiom-team.fr/mediatheque).

**Jouer un rôle de passerelle entre développeurs et utilisateurs**, notamment en administrant un forum public de réflexion, de débats et d'annonces ; ainsi qu'un forum également public mais dédié aux échanges techniques.

### Antennes

**Tisser un réseau d'entraide et accompagner les groupes locaux qui le souhaitent**. Axiom rend des services aux groupes locaux qui souhaitent devenir des antennes, par exemple pour l'organisation d'événements. Si vous ne souhaitez pas créer d'association, vous pouvez utiliser Axiom-Team pour effectuer un portage administratif.

Pour faire votre demande et devenir une [antenne](https://axiom-team.fr/antennes), il vous suffit d'envoyer un mail à <contact@axiom-team.fr> Merci de nous excuser si nous mettons quelques jour pour vous répondre, nous sommes tous bénévoles.

Nous accueillons les antennes lors d'une visio mensuelle qui leur permet d'échanger sur les besoins et les projets et de rencontrer les autres antennes.

::featured-articles
::

## Plusieurs façons de contribuer

Vous pouvez contribuer avec **un don en June (Ǧ1)** sur le compte d'Axiom Team.
_Clé publique_ :
[8CWuf4f1jYoVzHh4DEFpCyzZYC1pgz4t2wU8F2zKCthh](https://g1.duniter.fr/#/app/wot/8CWuf4f1jYoVzHh4DEFpCyzZYC1pgz4t2wU8F2zKCthh/)

Vous pouvez également contribuer **en euros**.
En participant aux campagnes de financements que nous menons ponctuellement (crowdfunding comme celui de la fin 2022 que nous avons baptisé Duniter V2), ou sous forme d'un don récurrent fléché (comme celui pour Gecko ou ceux que nous lançons maintenant), ou encore sous forme d'un don totalement libre.

Vous pouvez également [adhérer à l'association](https://www.helloasso.com/associations/axiom-team/adhesions/adhesion-2024-axiom-team).

Vous trouverez toutes ces options sur notre page [Hello Asso](https://www.helloasso.com/associations/axiom-team/).
Si vous ne souhaitez pas utiliser cette plate-forme, vous pouvez envoyer un chèque à : Axiom Team, 14 rue Johannes Kepler 31500 Toulouse.
Effectuer un virement est également possible, sur ce [RIB](https://cloud.axiom-team.fr/s/Gy7a54T36yYrQz9?dir=undefined\&path=%2F\&openfile=504938).

[Faire un don](https://www.helloasso.com/associations/axiom-team/formulaires/2)

![](/uploads/tirelire.png){width="400"}

Nous vous remercions d'avance pour toutes vos contributions, elles sont très nécessaires et précieuses. Elles peuvent également ne pas être que financières.

vous pouvez par exemple nous aider, en vous mobilisant de façon plus engagée, de façon ponctuelle ou pour une plus longue durée. Vos soutiens et le partage des très nombreuses tâches de l'association sont vitaux pour l'association et surtout, pour nous. C'est encore plus précieux.
