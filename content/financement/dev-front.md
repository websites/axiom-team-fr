---
title: Dev-front
---

# DESC

[ DECENTRALIZED EUROS SERVICES CIRCLE_]{.bg-yellow-500}

Une application au service des devs pour l'auto-arbitrage décentralisé de vos dons par les cercles de répartition.&#x20;

_Aujourd'hui greffée sur une gestion bancaire et administrative de l'association Axiom-Team.

## Fléchage : Développements des applications clientes, utilitaires et extensions, dits frontend
