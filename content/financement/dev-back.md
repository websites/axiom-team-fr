---
title: Dev-back
---

# DESC

[ DECENTRALIZED EUROS SERVICES CIRCLE_]{.bg-yellow-500}

Une application au service des devs pour l'auto-arbitrage décentralisé de vos dons par les cercles de répartition.&#x20;

_Aujourd'hui greffée sur une gestion bancaire et administrative de l'association Axiom-Team.

## Fléchage : Développements de l'écosystème technique dit backend, administration de l'infrastructure
