---
title: Test
description: test
---

# DESC

[ DECENTRALIZED EUROS SERVICES CIRCLE_]{.bg-yellow-500}

Une application au service des devs pour l'auto-arbitrage décentralisé de vos dons par les cercles de répartition.&#x20;

_Aujourd'hui greffée sur une gestion bancaire et administrative de l'association Axiom-Team._

Elle vous permet d'exprimer une intention ou une priorité pour l'usage de vos dons, les fléchages définis par les cercles de devs. Mais nous commençons naturellement le don le plus beau : le don libre.

Il ne signifie pas que l'asso en fait bien ce qu'elle veut, cela permet aux devs de s'adapter aux besoins du moment, d'alimenter un besoin qui ne l'est pas assez, bref, il permet la souplesse. &#x20;

## Don Libre&#x20;

::cartouche
---
conso: 6878
date: 12/12/2023
description: Redistribution par axiom-team
montant: 8909
nombreDeDons: 789
numeroOdoo: 34
title: Dons libres
---
Praesent nec nisl a purus blandit viverra. Vivamus quis mi. Curabitur ullamcorper ultricies nisi.
Curabitur turpis. Integer tincidunt. Nunc sed turpis.

  :::progress-bar
  ---
  total: 9087
  value: 7678
  ---
  :::

  :::progress-bar
  ---
  step: 200
  total: 9087
  value: 7678
  ---
  :::

  :::progress-bar
  ---
  step: 50
  total: 9087
  value: 7678
  ---
  :::

#after
### Consommation

  :::progress-circle
  ---
  description: "Montant dépensé :"
  total: 8909
  value: 6788
  ---
  Suite à l'AG du 12/12/23, la répartition du budget est la suivante :
  
  - infra
  - comm
  :::

  :::progress-circle
  ---
  description: "Montant dépensé :"
  total: 8909
  value: 6788
  ---
  Suite à l'AG du 12/12/23, la répartition du budget est la suivante :
  
  - infra
  - comm
  - [Lien vers le compte-rendu](https://lienverscompterendu.com)
  :::
::
