---
title: "Mentions Légales – Conditions d’utilisation (CGU) - RGPD "
description: "Mentions Légales – Conditions d’utilisation (CGU) - RGPD "
---
Mentions Légales – Conditions d’utilisation (CGU) - RGPD 

**Mentions Légales**

Création et infogérance du site : Manutopik – Yvv – HugoTrentesaux – Poka 

Propriétaire du site : Axiom-Team -  Association loi 1901 dont le siège social est situé 14, rue Johannes Kepler – 31500 TOULOUSE - RNA W313032210 – SIRET 85009663700010

Directeur de la publication : Manutopik - contact\[@]axiom-team.fr

Ce site est hébergé par : à remplir ???

**Conditions Générales d’Utilisation (CGU)**

ARTICLE 1 : Objet
Les présentes conditions générales d’utilisation ont pour objet l’encadrement juridique des modalités de mise à disposition des services du site axiom-team.fr et leur utilisation par « Les Visiteurs.euses ». Les conditions générales d’utilisation doivent être acceptées par tout Utilisateur.trice souhaitant accéder au site. Elles constituent le contrat entre le site et l’Utilisateur.trice. L’accès au site par l’Utilisateur.trice signifie son acceptation des présentes conditions générales d’utilisation. En cas de non-acceptation des conditions générales d’utilisation stipulées dans le présent contrat, l’Utilisateur.trice se doit de renoncer à l’accès des services proposés par le site axiom-team.fr et le site axiom-team.fr se réserve le droit de modifier unilatéralement et à tout moment le contenu des présentes conditions générales d’utilisation.

ARTICLE 2 :
Mentions légales (voir en début de page)

ARTICLE 3 : Définitions
La présente clause a pour objet de définir les différents termes essentiels du contrat : Utilisateur.trice : ce terme désigne toute personne qui utilise le site ou l’un des services proposés par le site. 

ARTICLE 4 : Accès aux services
Le site permet à l’Utilisateur.trice un accès gratuit aux services suivants : articles d’information ; réception de la « Newsletter ». Le site est accessible gratuitement en tout lieu à tout Utilisateur.trice ayant un accès à Internet. Tous les frais supportés par l’Utilisateur pour accéder au service (matériel informatique, logiciels, connexion Internet, etc.) sont à sa charge. Tout événement dû à un cas de force majeure ayant pour conséquence un dysfonctionnement du réseau ou du serveur n’engage pas la responsabilité d’axiom-team.fr. L’accès aux services du site peut à tout moment faire l’objet d’une interruption, d’une suspension, d’une modification sans préavis pour une maintenance ou pour tout autre cas. L’Utilisateur s’oblige à ne réclamer aucune indemnisation suite à l’interruption, à la suspension ou à la modification du présent contrat. L’Utilisateur a la possibilité de contacter le site par messagerie électronique à l’adresse contact\[@]axiom-team.fr

ARTICLE 5 : Propriété intellectuelle
Les marques, logos, signes et tout autre contenu du site font l’objet d’une protection par le Code de la propriété intellectuelle et plus particulièrement par le droit d’auteur. L’Utilisateur.trice sollicite l’autorisation préalable du site pour toute reproduction, publication, copie des différents contenus. L’Utilisateur.trice s’engage à une utilisation des contenus du site dans un cadre strictement privé. Une utilisation des contenus à des fins commerciales est strictement interdite sans demande préalable acceptée. 

ARTICLE 6 : Données personnelles 
voir  l’onglet RGPD 

ARTICLE 7 : Responsabilité et force majeure
Les sources des informations diffusées sur le site sont réputées fiables. Toutefois, le site se réserve la faculté d’une non-garantie de la fiabilité des sources. Les informations données sur le site le sont à titre purement informatif. Ainsi, l’Utilisateur.trice assume seul l’entière responsabilité de l’utilisation des informations et contenus du présent site. Une garantie optimale de la sécurité et de la confidentialité des données transmises n’est pas assurée par le site. Toutefois, le site s’engage à mettre en œuvre tous les moyens nécessaires afin de garantir au mieux la sécurité et la confidentialité des données.

ARTICLE 8 : Liens hypertextes
De nombreux liens hypertextes sortants sont présents sur le site, cependant les pages web où mènent ces liens n’engagent en rien la responsabilité de axiom-team.fr qui n’a pas le contrôle de ces liens. L’Utilisateur.trice s’interdit donc à engager la responsabilité du site concernant le contenu et les ressources relatives à ces liens hypertextes sortants.

ARTICLE 9 : Évolution du contrat
Le site axiom-team.fr se réserve à tout moment le droit de modifier les clauses stipulées dans le présent contrat.

ARTICLE 10 : Durée
La durée du présent contrat est indéterminée. Le contrat produit ses effets à l’égard de l’Utilisateur.trice à compter de l’utilisation du service.

ARTICLE 11 : Droit applicable et juridiction
Droit applicable et juridiction compétente la législation française s’applique au présent contrat. En cas d’absence de résolution amiable d’un litige né entre les parties, seuls les tribunaux du ressort de la Cour Administrative d’Appel de Toulouse sont compétents.

**RGPD – Règlement Général sur la Protection des Données**

axiom-team.fr accorde une importance particulière au respect de la vie privée des Utilisateurs.trices et de la confidentialité de leurs données personnelles et s’engage ainsi à traiter les données dans le respect des lois et réglementations applicables, et notamment la Loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés et le Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l’égard du traitement des données à caractère personnel et à la libre circulation de ces données (RGPD). Nous avons mis en place la procédure exigée par la loi du 25 Mai 2018, dites RGPD (règlement général sur la protection des données) que vous trouverez sur le site de la CNIL

Nous recueillons les informations que vous voulez bien nous fournir pour :

* L’abonnement à la Lettre d’Info du site (Newsletter)
* Lors de votre adhésion ou don à l’association Axiom-team

Vos données personnelles : nom(s), prénom(s), adresse de domicile, adresse de courriel, téléphone, sont stockés de façon chiffré, personne n’a accès à ces données. Nous ne donnons, ni louons, ni vendons, ni échangeons vos données.
Vous pouvez à tout moment nous demander de supprimer ces données de notre serveur sur simple demande à cette adresse : contact\[@]axiom-team.fr, cette suppression sera définitive.

Pour la Newsletter, il suffit de cliquer sur « Se Désabonner » en bas de la Lettre d’Info, la suite est automatique.
Attention, toutes vos informations et vos données seront perdues et ni vous, ni nous ne pourrons les récupérer, l’effacement est définitif.

Le pilote désigné pour gérer les données de ce site est : Yvv - Pi - Manu – contact\[@]axiom-team.fr