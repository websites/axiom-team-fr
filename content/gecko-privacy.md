---
title: Politique de confidentialité de Ğecko
description: Politique de confidentialité de Ğecko
---

Ğecko est une application de paiement décentralisée permettant d'effectuer des transactions en monnaie Ğ1. Cette application est un logiciel libre vous garantissant les quatre libertés fondamentales : utilisation, étude, modification et redistribution du code source.

## Données collectées et stockage

- Les seules données techniques collectées sont les logs de connexion (adresse IP, User-Agent, horodatage)
- Les clés privées sont stockées exclusivement en local sur votre appareil
- Les transactions sont enregistrées sur la blockchain Ğ1, accessible publiquement, ainsi que par les indexeurs et tout nœud du réseau
- Les permissions demandées (caméra, photos) servent uniquement au scan de QR codes et à la gestion des avatars

## Traitement des données

- Les transactions sont publiques par nature sur la blockchain Ğ1 et ses indexeurs
- Les clés privées restent strictement sur votre appareil
- Aucune donnée n'est vendue ou partagée avec des tiers, hormis les informations publiquement accessibles sur la blockchain

## Sécurité

- Chiffrement local des données sensibles
- Aucun stockage cloud des clés privées
- Protection par code PIN

## Vos droits

- Contrôle total sur vos données locales
- Possibilité de supprimer l'application et toutes les données stockées localement
- Les données inscrites sur la blockchain ne peuvent être ni modifiées ni supprimées, conformément à sa nature immuable

## Code source

Ğecko est un logiciel libre sous licence GNU GPL v3. Le code source est disponible sur [notre dépôt Git](https://git.duniter.org/clients/gecko).

## Contact

Pour toute question relative à la confidentialité : [contact@axiom-team.fr](mailto\:contact@axiom-team.fr)

_Document rédigé avec l'assistance de Claude 3 - Décembre 2024_
