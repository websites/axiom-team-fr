---
title: Hugo Trentesaux
role: développeur
thumbnail: /uploads/HugoTrentesaux.png
pubkey: 55oM6F9ZE2MGi642GGjhCzHhdDdWwU6KchTjPzW7g3bp
mail: hugo@trentesaux.fr
phone: +33 6 49 88 18 21
social_networks:
  - name: mastodon
    link: https://mastodon.zaclys.com/@h30x
---

Je suis Hugo Trentesaux. Je m’intéresse à la Ğ1 depuis 2017 et pense que l’association Axiom Team constitue une base juridique utile car nécessaire pour de nombreuses interactions avec le monde €. J’ai travaillé sur le dossier de financement de Ǧecko auprès de l’ADEME avec succès.
À l’avenir, je compte participer au fonctionnement d’Axiom Team, et à la partie rédactionnelle des dossiers de financement.
