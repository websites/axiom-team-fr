---
title: Sarah Marty
thumbnail: /uploads/sarah.jpg
pubkey: 29UgQ7qrQ4HcJEM3XPLJrMYbnsCLxXEmnx1EnDbcPra5:Gtw
---
Nomade dans l’âme, je suis une voyageuse positive et enthousiaste. J’aime découvrir de nouvelles cultures et connecter les informations et les personnes entre elles.

Je m'intéresse tout particulièrement aux projets qui ont à cœur la préservation de l'environnement, la condition féminine et qui ont un impact pour créer un monde meilleur.

Ma devise préférée :

" Le monde est un beau livre, mais il sert peu à ceux qui ne peuvent pas le lire ".