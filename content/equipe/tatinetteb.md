---
title: tatinetteb
role: "adhérente Axiom team "
thumbnail: /uploads/elisabeth.jpg
pubkey: q1wzQ5KvSBDHcFRk5SKrH7KaSDHv3JUbzRQd3XF6m4J:G2r
---
J'ai pris conscience que nous sommes présents en tant qu'utilisateurs, mais derrière il y a des développeurs et j'ai eu envie d'apporter mon aide. Dans Axiom  Team j'ai découvert des développeurs à l'écoute et accessibles pour les junistes.

Je suis apprentie forgeron et aide technique