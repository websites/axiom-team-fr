---
title: Étienne Bouché
thumbnail: /uploads/poka.svg
mail: poka@p2p.legal
---

Je suis contributeur actif sur le projet Duniter depuis 2016 aux RML7 de Laval.

Je code Ğecko en Flutter/Dart.
Je maintiens aussi l’infra Axiom-Team, soit 2 serveurs ProxMox.

J’ai aussi codé Ğ1-stats en bash.
Et jaklis en python.
J’ai aussi codé py-g1-migrator

J’aime la soupe au poireaux.

edit important: J’aime aussi les tartes aux légumes. Je préfère parler de tarte plutôt que de quiche aux légumes, même si j’aime beaucoup les quiches aussi. Je n’ai aucune animosité particulière vie-à-vis des gens qui préfèrent parler de quiche aux légumes plutôt que de tartes, même si je les considères comme éloignés de moi (au niveau de la race je veux dire hein, pas de malentendu).

Je n’aime pas le cresson.