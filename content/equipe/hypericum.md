---
title: Daniel
thumbnail: /uploads/hypericum.png
---

Donc moi c'est Daniel.
Je m'intéresse à pas mal de choses, dont les logiciels libres et éthiques (libre n'est pas toujours éthique!), et suis arrivé à la Ğ1 via une page de framasoft (celle-ci https://degooglisons-internet.org/fr/alternatives/ ), et donc: "mais quel est cet étrange "duniter" en face de paypal??"
J'ai été pas mal dans différents activismes, et ce qui m'a plu dans duniter (par rapport à bitcoin dont framasoft parle aussi), c'est cette forme de solidarité "automatique" inscrite dans le code, et la nécessité de rencontrer des gens.
J'ai étudié la biologie, n'ai pas réussi sur le moment à trouver quelque chose qui m'a bien convenu et où je me sentais bien (convictions, social, créativité, etc…), puis en 2011 je suis rentré dans une entreprise de livraison à vélo (qui me permet beaucoup de souplesse et d'avoir plus de temps libre, et le vélo était de toute façon une de mes passions!), du coup je livre surtout des choses dans le médical (ce qui est amusant, car je peux surprendre parfois les médecins, techniciens et pharmaciens avec qui je bosse ,o)
J'aime bien réinventer la roue, essayer de comprendre en profondeur, j'apprécie beaucoup la notion de communs, ou de bien communs, me soucie des questions de décisions partagées et de diminuer la quantité de hiérarchies qui existent.
Je me suis dit que je suis moyen dans pas mal de domaines qui nous intéressent ici, pas spécialement bon dans les trucs techniques (du code je peux apprendre mais pour l'instant j'en suis à des notions rudimentaires de bash), pas dans les langues non plus (allemand en premier mais un peu hésitant, puis anglais), et les questions administratives là je peux dire que je ne suis pas à l'aise (même si j'estime que je devrai aussi l'apprendre).

Ce que je me suis dit que je pourrais apporter, c'est aider à la vulgarisation et aux documentations, pour que plus de monde puisse se plonger plus facilement dans les aspects techniques même modestes (avoir un noeud miroir par exemple).