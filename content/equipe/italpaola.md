---
title: Paola
thumbnail: /uploads/paola.jpg
pubkey: DBgJpBWVHA2WcXCVuTB4qTGj4wWzZqsR6JUV4DMdBoDC
mail: ""
social_networks:
  - name: telegram
    link: https://t.me/italpaola
---
Citoyenne du monde, œuvrant pour **le libre et la liberté** : d’être, de penser, d’agir, de se relier, de construire un monde libre respectueux de l’humain et du vivant. 

Dans la joie, la paix, le bonheur, la communication non violente, la co-création monétaire humaine libre. 

Juniste depuis 2021, j’adore les Ğmarchés et je soutiens l’écosystème technique de DUNITER et ses développeurs comme je peux ;-)