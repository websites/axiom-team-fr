---
title: Attilax
thumbnail: /uploads/attilax.jpg
pubkey: EAHgNyYsxmS7YSfuQsEzEWwKnD2UyMzwVTuBnstMSk3e
---

Attilax

Comédien, libriste, touche à tout, j’ai naturellement intégré axiom quand j’ai voulu m’investir pour la june comme je m’étais investi pour le logiciel libre. Pi, Poka, Hugo (entre autres) sont devenus de vrais potes.
J’ai fait plein de logos, organisé des events, des voyages de la june, des conférences, les rencontres à Paris pendant 3 ans, lancé les projets infojune, juneland, g1formation (hélas dead), g1marché (dead aussi) et je compte bien continuer à faire tout ce que je peux pour aider la G1 et ses contributeurs à s’épanouir. J’auto-héberge aussi des backups (du forum ML notamment), le peertube de Poka et le infojune & wotwizard de Paidge. Bref, j’aime dire et faire n’importe quoi d’une manière générale. Je crois à la chance, au chaos, et surtout, en nous.