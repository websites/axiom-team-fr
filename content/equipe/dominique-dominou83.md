---
title: "Dominique "
role: Trésorière UNL
thumbnail: /uploads/dominou.jpg
pubkey: F9R3AdWM4xCyuPAk5exRyuiDcEQZJ2PVdanThoaG7Rv:F9m
mail: ""
---
Bonjour à tous-tes

Je me prénomme Dominique, incarnée dans un corps de femme.

J’ai découvert la June en 2021. J’ai mis 9 mois à m’informer car je me disais “c’est trop beau” s’il n’y a rien à payer. Puis  j’ai participé à l’Université d’été de juillet 2022. J’y ai vécu un bain d’Amour, d’entraide, de bienveillance, de respect. Tous mes doutes se sont envolés, j’ai trouvé ma tribu, découvert des humains me ressemblant.

Et je me suis posée la question : comment puis-je contribuer ? Avec la Monnaie Libre j’ai découvert des compétences dont je n’avais pas conscience. J’adore danser, cuisiner pour partager, aider à devenir plus autonome, pratiquer des soins énergétiques, la permaculture dans tous les sens du terme, aider dans l’administratif, tenir un bar,  voyager (surtout à vélo) pour découvrir des humains.

Et tout naturellement j’ai rejoint Axiom-Team en 2023. Depuis février 2024 je suis la trésorière UNL  de l’association.