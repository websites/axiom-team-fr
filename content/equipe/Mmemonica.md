---
title: "Monique Laleeuwe "
thumbnail: /uploads/monica.jpg
pubkey: HJ9HKMsynq2KQQzJDd6YUt9yUbr6GpmnQPfAkGFZ8ht1
social_networks:
  - name: globe
    link: https://linktr.ee/MoniqueLaleeuwe
  - name: youtube
    link: https://youtube.com/@madamemonica9251
  - name: youtube
    link: https://youtube.com/@Laroutedurythme
---
Toute une histoire.
Slasheuse dans toute sa splendeur ! 
Musicienne-Coach-Formatrice-Humoriste-Journaliste web-community manager-global artistic coach.... Artiste dans l'âme (musique et théâtre), j'ai toujours cru en un monde plus juste, plus équitable depuis toute petite. 
Un exemple : je confectionnais des colis solidaires pour ceux et celles qui n'arrivaient pas à manger à leur faim lors des cours de morale à l'école.
J'en ai fait ma figure de proue, mon cheval de troie 😉. 

J'ai croisé le chemin de la june, des junistes et des Axiom-teamers en 2019.
1er rendez-vous raté... Il a fallu attendre mars 2023 pour que la sauce prenne. Et la, je me suis investie quasi tout de suite dans ce nouveau monde que j'attendais tant. 

J'ai rejoint Axiom-Team pour ses valeurs, ses actions et ses membres passionnés et passionnants. J'y partage mes compétences de Community Manager.  Chaque jour, un nouveau défi tant personnel que relationnel. 

La monnaie libre, c'est aussi la liberté d'évoluer ensemble sur différents chemins. 

Au plaisir de vous rencontrer