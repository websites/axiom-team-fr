---
title: Yvv
role: Secrétaire d'époque ... passée.
thumbnail: /uploads/yvv.png
---
En que vieux bouc dans le CA, je tire ma révérence. Focus sur ce qui m'intéresse maintenant, nouvelle forme de mobilisation. 

Pour mission UNL
* aboutir la tuyauterie autogestion des dons
* l'élargir pour une v2 sur ... un goût de paradis.

Pour mission fédération - services aux monnaie-libristes
* bosser sur une FAQs version wiki, si un mediawiki ou autre voit le jour.
* bosser sur une médiathèque, si un nocodb ou autre voit le jour.

Pour ML
* lancer un événement structurant.
* lancer une expérience de production collective juniste.
* reprendre mes 2 bouquins et mon blog que j'ai misérablement abandonnés.