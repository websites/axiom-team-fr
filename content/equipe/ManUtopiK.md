---
title: Emmanuel Salomon
thumbnail: /uploads/ManUtopiK.jpg
pubkey: 2JggyyUn2puL5PG6jsMYFC2y9KwjjMmy2adnx3c5fUf8
mail: emmanuel.salomon@gmail.com
phone: +33 6 52 22 40 33
social_networks:
  - name: gitlab
    link: https://git.duniter.org/manutopik
  - name: github
    link: https://github.com/ManUtopiK
  - name: twitter
    link: https://twitter.com/manutopik
---
Diplomé dans le domaine des énergies renouvelables, mon côté _"web enthousiaste"_ m'a finalement amené à faire du développement web depuis + de 12 ans.

Passionné par tout ce qui est "alternatif" et qui rend libre, j'ai découvert le concept de la monnaie libre en 2014.
L'économie actuelle est à mes yeux le principal facteur du bordel que l'on a mis sur cette planète depuis des générations. J'espère en un monde un peu plus libre, auto gouverné en intelligence collective, et avec du #solarpunk comme horizon.
Profitons des crises pour tout changer !
