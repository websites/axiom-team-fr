---
title: Sylvestre MIGNOT
role: Secrétaire Axiom-Team
thumbnail: /uploads/syoul.jpeg
pubkey: F9JEPzo3sE9LtVNx6u2EcriPzzgnbXtUnvUtjQRDcTeu
---
Actuellement secrétaire d'Axiom-Team

Artisan bidouilleur
Libriste, formé à la bidouille (résilience numérique, énergétique, domotique). Artisan laser numérique sur le causse du Querçy (46)

J'ai découvert la June en 2018. Depuis, j'anime avec des groupes locaux, des conférences et Ğmarchés autour de la monnaie libre G1.