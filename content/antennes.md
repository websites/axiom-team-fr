---
title: Antennes
description: Les antennes locales d'Axiom-Team
---

## Les antennes locales d'Axiom-Team

Axiom rend des services aux groupes locaux qui souhaitent devenir des antennes, par exemple pour l'organisation d'événements. Si vous ne souhaitez pas créer d'association, vous pouvez utiliser Axiom-Team pour effectuer un portage administratif.

Vous pouvez disposer notamment de la couverture d'assurance que fournit l'association Axiom-Team. Vous disposez également du support et de l'entraide du réseau monnaie libre, et l'accompagnement dans les démarches administratives s'il y en a.
Pour faire votre demande, il vous suffit d'envoyer un mail à <contact@axiom-team.fr> Merci de nous excuser si nous mettons quelques jour pour vous répondre, nous sommes tous bénévoles.

Nous accueillons les antennes lors d'une visio mensuelle qui leur permet d'échanger sur les besoins et les projets et de rencontrer les autres antennes.

### Présentation des antennes

##### Axiom Team PACA

La dernière née...
