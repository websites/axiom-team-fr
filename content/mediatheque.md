---
title: Médiathèque
description: "L'une des missions d'axiom est de fournir à tous les utilisateurs
  des supports de communication, exploitable pour les besoins de présentations
  ou des groupes locaux. "
---

Nous rassemblerons dans cette médiathèque tous les visuels qui ont été réalisés et que les auteurs ont bien voulu mettre à disposition.

Vous trouverez des visuels, des tracts, des présentations, des affiches, des vidéos, des tutos, des fichiers 3D, et autres supports. En attendant, ils sont actuellement sur le cloud de l'asso, ce n'est pas très ergonomique en l'état mais malgré tout disponible [ici](https://cloud.axiom-team.fr/s/DDbKi2pqdEmixS9).

::alert{icon="heroicons-information-circle-20-solid" type="info"}
En cours de construction...
::
