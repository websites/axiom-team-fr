---
title: Nouveau site
description: Nous avons refait le site, non seulement parce que la version
  précédente était obsolète, mais également pour changer l'environnement
  d'administration des contenus.
date: 2023-01-01
---

Pour la mise en ligne du nouveau site, cet article de blog écrit en décembre 2022.

Techniquement nous sommes passés du CMS de Odoo à Nuxt.
