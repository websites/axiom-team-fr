---
title: La Ḡ1 gagnante du prix de l’innovation Open Source 2019
thumbnail: /uploads/blog/slide-geant-opensource-summit.jpg
date: 2019-12-01
---

Le CNLL (Union des Entreprises du Logiciel Libre et du Numérique Ouvert) annonce les lauréats du concours des Acteurs du Libre 2019, en partenariat avec Paris Open Source Summit, co-organisateur du concours, la communauté OW2 et l’ADULLACT.

"Le prix de l’innovation Open Source est décerné à l’association Axiom-Team pour l’innovation éthique et écologique de son projet de monnaie libre Ḡ1 (« June »), et pour son soutien à l’équipe de développeurs du logiciel Duniter et de son écosystème, moteurs de cette monnaie libre comprenant déjà plus de 2300 co-créateurs."

Bravo à toute l’équipe des développeurs de Duniter pour cette super innovation désormais reconnu encore un peu plus comme telle par nos pairs !

Bravo aussi à toute la communauté ḡ1 et ses groupes locaux pour faire vivre cette monnaie !

Sources:

- <https://lesacteursdulibre.com/>
- Global Security Mag Online <http://www.globalsecuritymag.fr/Le-CNLL-revele-les-laureats-du,20191210,93727.html>

::carousel{collection="prix-innovation" height="400" items="3"}
::

::grid{collection="prix-innovation" items="2"}
::
