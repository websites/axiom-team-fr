---
title: L'ADEME finance Ğecko et Duniter
description: L'ADEME a lancé en mars 2021 un appel à projet sur le thème des
  communs. Son objectif est de favoriser la résilience des territoires via des
  projets collectifs ayant un impact à l'échelle locale. Parmi 247 communs
  recensés, Ğecko a été sélectionné pour financement.
thumbnail: /uploads/divers/logoademe2020_rvb-150x150.webp
date: 2022-09-15
authors:
  - AxiomTeam
category:
  - Financement
---

::alert{type="info"}
Cet article a été publié initialement sur <https://duniter.fr/blog/financement-ademe/>
::

L'<abbr title="Agence De l'Environnement et de la Maîtrise de l'Énergie">[ADEME](https://www.ademe.fr/)</abbr> a lancé en mars 2021 un appel à projet sur le thème des communs. Son objectif est de favoriser la résilience des territoires via des projets collectifs ayant un impact à l'échelle locale. La méthodologie de l'appel à projet a été adaptée au thème des communs via la mise en place d'un wiki ([wiki.resilience-territoire.ademe.fr](https://wiki.resilience-territoire.ademe.fr/)) et d'un forum ([forum.resilience-territoire.ademe.fr](https://forum.resilience-territoire.ademe.fr/)). Cela a permis de faire émerger en toute transparence les projets suffisamment structurés qui répondaient aux critères d'ancrage territorial et d'impact sur leur résilience.

L'association [Axiom Team](https://axiom-team.fr/), qui a pour but le développement technique de la Ğ1 a répondu à l'appel en rédigeant un dossier centré autour de l'application Ğecko [disponible sur le wiki](https://wiki.resilience-territoire.ademe.fr/wiki/Ğecko). Ce dossier a été écrit avant la décision de la [migration Duniter Substrate](https://duniter.fr/blog/duniter-substrate/) et était donc focalisé sur la conception d'une application de paiement suffisamment rapide pour répondre aux besoins des Ğmarchés. En effet, bien que Duniter soit indispensable au fonctionnement de la Ğ1, son ancrage local était moins évident que celui de Ğecko.

Parmi 247 communs recensés sur le wiki, Ğecko a été sélectionné pour financement. Vous pouvez consulter le détail du dossier sur le wiki ou en lire un résumé ci-dessous.

<img alt="illustration" src="/uploads/divers/ademe-finance-gecko.png" width="50%"/>

## Objet

Le développement impressionnant de l'économie en Ğ1 mérite l'évolution des outils de manière à ce que l'outil numérique n'en freine pas l'adoption. Parmi les objectifs du projet Ğecko, il y a :

- une connexion au réseau transparente : pas besoin de choisir de nœud
- un paiement rapide par simple scan, le montant pouvant être intégré au QR code
- une confirmation de paiement rapide, pas de fork ou de nœud désynchronisé
- une sécurité forte par code pin, pas d'identifiants interminables à saisir pour payer
- une capacité de charge plus élevée, pas de nœud saturé

Ces améliorations devraient fluidifier largement les échanges, en particulier sur les Ğmarchés. Bien entendu, cela repose toujours sur une connexion internet et en zone blanche il faudra se contenter de monnaie papier ou autre.

### Côté Duniter

Si certaines de ces améliorations peuvent avoir lieu côté client, la plupart nécessitent de grands changements côté serveur. C'est pourquoi le projet Ğecko vise également à développer des fonctionnalités dans Duniter. Idéalement, nous pourrions arriver au terme du projet Ğecko au moment du lancement de la Ğ1v2, déjà annoncée lors de l'université d'été de la monnaie libre 2022.

## Budget

Le coût du projet a été estimé à 50 000 euros, dont une partie fléchée vers Ğecko _frontend_ au sens strict et une autre vers les fonctionnalités _backend_ nécessaires à Ğecko (Duniter, indexeurs, données hors chaîne...). L'aide de l'ADEME couvrira 70 % de ce budget, soit 35 000 €, le reste étant assuré par auto-financement. Cette partie d'auto-financement peut être comptabilisée en monnaie ou en nature (par exemple par valorisation d'heures de travail bénévoles).

### Bénévolat

Bien qu'il nous semble important de valoriser le travail bénévole, son estimation rigoureuse est compliquée. Plusieurs milliers d'heures de travail qualifié ont été nécessaires pour en arriver à l'état actuel de Ğecko, soit plus que le montant total du projet, et cela sans compter les nombreuses heures d'apprentissage en autodidacte ni le travail côté Duniter. De plus, le travail bénévole se fait majoritairement sur du temps libre en parallèle d'une activité rémunérée, c'est-à-dire en soirée et le weekend. Pour cette raison, il nous semble important de procéder à un autofinancement en monnaie dette de manière à obtenir ce que ne permet pas le bénévolat et le financement en Ğ1 : des contributeurs à plein temps.

### Financement participatif

Pour les raisons évoquées ci-dessus et parce que cela nous a été demandé à plusieurs reprises lors des derniers événements publics, nous allons lancer un financement participatif auprès de la communauté Ğ1 et à destination du collectif de développeurs. Nous espérons ainsi libérer de leur "boulot euro" ceux qui le souhaitent parmi nos contributeurs techniques bénévoles. La structure porteuse sera Axiom Team et la méthode de partage des dons sera décidée par la méthode des cercles de répartition. Si ce cercle alloue 15 000 € à Axiom Team, cela permettra de satisfaire la partie d'autofinancement. Sinon nous aurons recours à la valorisation de bénévolat.

---

Pour être tenu au courant des nouvelles relatives au financement, abonnez-vous à la [catégorie "Axiom Team"](https://forum.monnaie-libre.fr/c/associations/axiom-team/115) sur le forum monnaie libre !
