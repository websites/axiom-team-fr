---
title: Financement participatif 2024
description: Nous lançons un financement participatif pour terminer les logiciels v2 pour un déménagment de la Ǧ1 en mars 2025 !
date: 2024-10-14
featured: true
thumbnail: /uploads/blog/2024_financement-participatif.png
authors:
  - AxiomTeam
---

Bonjour à toutes et à tous,

Nous avons le plaisir de vous annoncer notre nouveau financement participatif. Celui de 2022 avait permis de poser les bases des la v2 avec Duniter et Ğecko. Il est maintenant temps de finaliser ces logiciels ainsi que Cesium v2 pour un grand déménagement en mars 2025 !

Vous trouverez ci-dessous le texte descriptif du financement comme disponible sur la plateforme HelloAsso.

L’équipe d’Axiom Team


## Texte HelloAsso

### Une nouvelle maison pour la Ğ1

La maison Ğ1 a vu ses premiers habitants arriver le 8 mars 2017. Ces 59 personnes liées par 551 certifications ont vu progressivement leur maison évoluer (initialement Duniter 1.0 et Cesium 1.0), se meubler (wotwizard, la wotmap...), et de nouveaux habitants emménager. Ainsi, nous sommes aujourd'hui plus de 8400 membres liés par plus de 102000 certifications et nous habitons actuellement Duniter 1.8.7 et Cesium 1.7.14 qui ont permis plus de 580000 transactions !

Cependant, dès juillet 2021, le besoin s'est fait sentir de déménager vers une nouvelle maison plus grande et capable d'accueillir plus d'habitants. Le projet de ré-écriture était né. Des financements en euro ont permis en 2022 d'en construire les fondations (Duniter v2), et nous avons aujourd'hui des murs et un toit. Cette nouvelle maison nous fait envie, mais elle est encore très inconfortable : il lui manque des portes et des fenêtres !

Notre objectif est de finaliser la version 2 des logiciels (notamment Cesium v2) pour un déménagement le 8 mars 2025, date anniversaire de la Ğ1 ! Nous avons besoin de votre soutien pour franchir cette étape cruciale dans l'aventure de la communauté et qui représente un tournant majeur dans le développement de son écosystème logiciel.

![](/uploads/blog/crowdfunding-2024-1280x720.png)
> La v2 des logiciels : une nouvelle maison pour la Ğ1 !

### Pourquoi avons-nous besoin de vous ?

Les financements de 2022 (participatif et ADEME) ont permis de construire le gros oeuvre, mais nous sommes arrivés à leur terme. La quantité de travail à fournir pour réaliser les finitions reste largement supérieure aux disponibilités des bénévoles. Sans votre aide, nous serions contraints de faire des concessions, soit sur la qualité des finitions, soit sur la date de livraison, ce que ni nous, ni les junistes ne souhaitons.

C'est pourquoi nous lançons ce financement participatif. Les fonds récoltés permettront de recruter des développeurs à plein temps pour finaliser le projet dans les délais tout en maintenant le niveau de qualité.

Votre aide, qu'elle soit financière ou simplement en partageant cette campagne, fera une énorme différence. Ensemble, nous pouvons faire de ce projet une réussite. 

### Description des postes de dépenses

1. **premier palier : 6000€** 3 mois à plein temps (nov-déc-jan) pour un développeur encadré par Kimamila (@BenoitLavenier) sur Cesium v2, absolument nécessaire pour avancer sur Cesium v2, sans ça il faudra se contenter des autres clients v2 (pour l’instant uniquement Ǧecko donc uniquement sur mobile)
1. **deuxième palier : 12000€** +3 mois à plein temps (nov-déc-jan) pour Benjamin Gallois qui a déjà fait ses preuves sur Duniter et Duniter-Squid, Duniter doit encore gagner en stabilité en confort d’utilisation pour être facilement installable par tous (par exemple paquet Yunohost)
1. **troisième palier : 18000€** +3 mois à plein temps (fév-mar-avr) pour Benjamin Gallois pour ajouter des fonctionnalités pratiques côté Duniter (comme une découverte réseau pour les endpoints rpc et indexeurs)
1. **quatrième palier : 24000€** +3 mois à plein temps (fév-mar-avr) pour Hugo Trentesaux qui sera au chômage dès décembre et risque d’aller chercher un autre travail sinon
1. **cinquième palier : 36000€** un fond de roulement indispensable pour que l’asso Axiom Team continue ses activités telles que décrites sur notre infolettre de rentrée

![](/uploads/blog/crowdfunding-2024-1920x250.png)
>Les logiciels v2, nouvelle maison pour la Ğ1

### Bénéficiaires finaux de la collecte

Les bénéficiaires des sommes collectées seront les développeurs cités plus haut, via l'association Axiom-Team. Les comptes de l'association sont publiés sur le forum monnaie libre dans la catégorie dédiée : https://forum.monnaie-libre.fr/c/associations/axiom-team/115.

### Porteurs du projet

Ce projet de financement participatif est à l'initiative d'Axiom-Team. Plus d'informations sont disponibles aux liens suivants :

- site web de l'association Axiom Team https://axiom-team.fr/
- texte du dernier financement participatif sur le blog Duniter https://duniter.fr/blog/financement-participatif/
- plan de communication sur la mise à jour v2 https://monnaie-libre.fr/maj-v2/
- post de suivi technique de Duniter v2 https://forum.duniter.org/t/suivi-du-projet/11792
- index des vidéos techniques https://forum.duniter.org/t/les-videos-duniter-v2s/11794

N'hésitez pas à vous exprimer sur les forums (https://forum.monnaie-libre.fr/ pour les sujets non techniques et https://forum.duniter.org/ pour les sujets techniques). Nous faisons un effort pour transmettre au mieux des concepts complexes et avons besoin de vos retours pour adapter nos explications.
