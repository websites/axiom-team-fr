---
title: Infolettre de rentrée
description: Cette infolettre a été envoyée à la rentrée de septembre 2024. Nous la rendons également accessible sur notre blog.
date: 2024-09-23
featured: true
thumbnail: /uploads/envelope.svg
authors:
  - AxiomTeam
---

Bonjour à toutes et à tous,

Après une longue période d’absence, nous sommes ravis de vous retrouver et de relancer notre newsletter ! Ces derniers mois ont été riches en défis et en réflexions, et nous avons pris le temps de nous réorganiser pour mieux définir et remplir les missions de l’association. Nous vous remercions de votre patience et de votre soutien, qui sont essentiels à la continuité de nos actions.

Dans cette édition, nous vous tiendrons informés des **projets en cours**, des **événements à venir** et des opportunités pour continuer à participer activement à la vie de notre association. Votre engagement et votre énergie sont le moteur de notre communauté, et nous sommes plus que jamais prêts à reprendre ensemble le chemin de l’action.

Au plaisir de vous retrouver lors de nos prochains rendez-vous !

## Dernières activités et état de l’association

**Duniter v2**, le gros chantier de développement logiciel soutenu par Axiom Team arrive en phase finale ! Cela veut dire que les outils produits ne seront plus uniquement à destination des développeurs, mais également des utilisateurs curieux. Restez à l’écoute si vous voulez tester Cesium v2 en avant-première (programmes “alpha” et “beta”).

La **rencontre Ğ1ntada** en Espagne a permis de réunir les développeurs français et espagnols représentés par Kapis et Hugo pendant la conférence visible sur https://tube.p2p.legal/w/fiGqdKifi2bYeXcgY1UVjS.

L’asso connait un coup de mou suite à la démission de son président et réfléchit à un **renouveau**. Parallèlement, plusieurs membres de l’asso se sont emparés du bilan des RML18 (cf vidéo https://tube.p2p.legal/w/633d0b31-8045-4863-9d09-15152302f293) pour participer à un groupe de coordination sur la mise-à-jour v2 (cf https://forum.monnaie-libre.fr/c/communication/mise-a-jour-v2/131).

Nous n’avons malheureusement pas été sélectionnés pour le **financement européen NGI** sur lequel nous comptions pour avancer sur les datapods v2 (suite des pods Cesium+).

L’association compte aujourd’hui 66 membres contre 125 l’année dernière à la même période. Nous proposons de nous rassembler pour une **Assemblée Générale Extraordinaire** du 8 au 11 novembre 2024 à Ramonville (Toulouse), le programme sera envoyé prochainement aux membres, n’hésitez pas à (ré-)[adhérer](https://www.helloasso.com/associations/axiom-team/adhesions/adhesion-2024-axiom-team) pour y participer.

Nous avons publié avec un peu de retard le bilan et compte de résultat 2023 comme d’habitude sur le fil de l’association sur le forum monnaie libre : https://forum.monnaie-libre.fr/c/associations/axiom-team/115.

En 2022 nous avons publié un premier [article sur le financement de Ğecko](https://axiom-team.fr/blog/2022-09-15-financement-ademe) par l’ADEME. Aujourd’hui au terme de ce financement, c’est le moment de faire un bilan sur l’aboutissement et l’évolution du projet. Suite de l’article [sur notre blog](https://axiom-team.fr/blog/2024-09-02-bilan-ademe).

## Projets futurs

Nous sommes heureux de vous annoncer le lancement d’une campagne de **financement participatif** dès **le 15 octobre 2024** pour soutenir la finalisation de Cesium v2 et Duniter v2. Ces deux projets sont essentiels pour renforcer et pérenniser l’écosystème de la monnaie libre. Votre soutien, qu’il soit financier ou par le partage de l’information, sera déterminant pour mener ces développements à terme.
La campagne se fera sur la plateforme HelloAsso (https://www.helloasso.com/associations/axiom-team), le lien sera diffusé sur notre site web et les réseaux de communication habituels (forums, Telegram…).

Nous comptons sur votre participation pour faire de cette campagne un succès et contribuer à l’avenir de la monnaie libre !

## Contributions bienvenues

Nous travaillons actuellement à la refonte de [notre site internet](https://axiom-team.fr/) et avons besoin de votre aide pour enrichir son contenu. Si vous avez des idées, des compétences en rédaction ou simplement l’envie de partager votre expérience, n’hésitez pas à vous manifester ! Chaque participation est précieuse pour faire de notre site un véritable reflet de notre association.
D’avance merci pour votre engagement !

## Le mot de la fin

Merci à toutes et tous pour votre attention et votre engagement continu. Nous espérons que cette newsletter vous a permis de mieux comprendre les projets en cours et les initiatives à venir. N’oubliez pas de rester connecté-e-s et de nous suivre de près pour contribuer à l’avancement de notre association. Ensemble, nous pouvons réaliser de grandes choses !

À très bientôt pour de nouvelles aventures !

L’équipe d’Axiom Team
