---
title: Bilan du financement ADEME
description: En 2022 nous participions à l'appel à commun de l'ADEME. Voici un
  bilan du projet qui montre l'évolution du projet sur cette période et les
  ouvertures possibles.
date: 2024-09-02
featured: true
thumbnail: /uploads/divers/logoademe2020_rvb-150x150.webp
authors:
  - AxiomTeam
category:
  - Financement
---

En 2022 nous avons publié un premier article sur le [financement de Ğecko par l'ADEME](/blog/2022-09-15-financement-ademe). Aujourd'hui au terme de ce financement, c'est le moment de faire un bilan sur l'aboutissement et l'évolution du projet.

## Rappel des objectifs et de la stratégie proposée

L'objectif de l'appel à commun de l'ADEME est de développer la résilience des territoires via des projets collectifs ayant un impact à l'échelle locale. L'association Axiom Team a répondu à l'appel en proposant l'application Ğecko, qui augmente la facilité et la rapidité des échanges de manière significative, pour les besoins des Ğmarchés.

Cet objectif requiert une application simple et rapide d'utilisation et des fonctionnalités serveur assurant une réactivité irréprochable. Les éléments techniques principaux permettant d'assurer ces objectifs sont :

- la création automatique et sécurisée de compte portefeuille
- la possibilité de payer sans mot de passe complexe
- une API réactive avec des délais inférieurs à la seconde
- un suivi réactif des transactions en attente

C'est pourquoi la stratégie proposée incluait à parts égales une partie backend (côté Duniter) et une partie frontend (côté application mobile).

## Objectifs atteints par une voie imprévue

Les développements frontend se sont déroulés sans encombre, mais ça n'a pas été le cas des développements backend. En effet, le développeur principal ayant commencé cette tâche a été recruté par une autre société, laissant une version non finalisée de Duniter 1.9 et une version préliminaire de Duniter 2.0. Nous n'avons pas trouvé le profil approprié pour prendre le relais, il nous a donc fallu trouver une stratégie alternative.

La voie astucieuse de la communauté bénévole :

1. Déployer la version instable de Duniter 1.9 en parallèle de Duniter 1.8.
2. Utiliser les briques techniques de l'application Ğecko pour développer une application compatible, Ğinkgo.
3. Transférer les efforts de Ğecko vers Duniter 2.0 pour finaliser son développement.

Les objectifs du financement ADEME ont été atteints via l'application Ğinkgo qui gagne aujourd'hui en adoption parmi la communauté Ğ1. Cette solution n'aurait pas pu voir le jour sans ce financement, mais une partie des développements ont eu lieu en dehors de ce cadre. Bel exemple de la souplesse permise par un logiciel libre mis à disposition d'une communauté.

## Développements réalisés et usages observés

Parmi les freins à l'utilisation de Cesium, il y a la gestion du mot de passe qui résulte d'un équilibre entre sécurité et facilité d'accès. Ğinkgo revoit cette question en générant un identifiant sécurisé pour l'utilisateur, qui lui permet de réaliser des transactions sans mot de passe, en reposant sur la sécurité du téléphone.
Cette innovation a transformé l'utilisation de la Ğ1 car les utilisateurs ont maintenant plusieurs portefeuilles "jetables", avec moins de monnaie et d'enjeu de sécurité, plus faciles à utiliser.

<style>
/* Manu, il faut que tu m'expliques comment insérer simplement des images, parce que là c'est juste pénible */
.grrrr {
    max-width: 300px;
    display: inline-block;
    border-radius: 0 !important; /* pourquoi est-ce que je suis obligé de faire ça ???? */
}
</style>

<img src="/uploads/blog/ginkgo-screenshots/pay.jpg" class="grrrr"/>
<img src="/uploads/blog/ginkgo-screenshots/receive.jpg" class="grrrr"/>

De plus, comme le montrent les captures d'écran ci-dessus, des images familières ont été utilisées pour que l'utilisateur puisse comparer son expérience dans l'application avec les moyens de paiement habituels.

Les échanges avec les animateurs de groupes locaux montrent que la prise en main des utilisateurs est simplifiée, ce qui permet d'accueillir de nouvelles personnes jusqu'alors rebutées par la rugosité de l'outil informatique.

## Nouveaux objectifs apparus lors du projet

Les objectifs annoncés ayant été atteints par Ğinkgo en cours de projet, nous avons continué sur notre lancée avec des objectifs plus ambitieux pour Ğecko. La solution actuelle peu pérenne, car dépendante d'une version de Duniter non maintenue, devait migrer pour stabiliser l'écosystème logiciel et permettre les développements futurs. Les efforts ont donc été concentrés sur la version 2 de Duniter ([\[1\]](https://duniter.fr/blog/duniter-substrate/), [\[2\]](https://duniter.fr/blog/duniter-v2-alpha/)).

Il s'agit d'une ré-écriture complète, un gros chantier. Mais Duniter sera mieux compris par les développeurs d'autres cryptomonnaies, ce qui est indispensable pour la maintenance à long terme du logiciel et le développement de nouvelles fonctionnalités.

Un aspect qui démarque Duniter d'autres projets est sa toile de confiance, un système d'identité numérique décentralisé à grande échelle aux applications multiples. Une fois la version 2 solidement établie, nous pourrons développer les applications permises par la toile de confiance :

- des systèmes de vote transparents et surs à toutes les échelles
- des plateformes d'échanges de services hautement dépendants de liens de confiance

Ces applications pourront faire l'objet de propositions pour des appels à communs futurs.

## Bilan sur l'impact du financement

Les échanges en Ğ1 connaissent une croissance stable (actuellement autour de 15.000 transactions par mois). Le nombre de régions concernées augmente [carte.monnaie-libre.fr](https://carte.monnaie-libre.fr/) et les Ğmarchés ont lieu avec régularité (agenda : [monnaie-libre.fr](https://monnaie-libre.fr/#agenda)).
Nous n'avons pas de système de surveillance permettant de tracer les utilisateurs de Ğinkgo, mais le nombre de commentaires de transaction publics incluant le mot "Ginkgo" a atteint des proportions similaires à celles de "Cesium", application la plus utilisée car c'était la première.

::grid{collection="2024-monthly-tx" height="400" items="3"}
::

Le nombre d'utilisateurs Ğinkgo est inférieur à celui de Cesium, mais la quantité de transactions est supérieure, du fait de la facilité d'utilisation de Ğinkgo.
RQ : ce chiffre est probablement sous-évalué car les transactions sur Ğinkgo sont moins souvent accompagnées d'un commentaire.

## Perspectives et ouvertures

L'impact de la diffusion d'applications intuitives est globalement toujours positif. Il serait néanmoins intéressant de mieux comprendre l'évolution des pratiques au niveau local. Pour cela, nous pourrions réaliser un travail documentaire consistant à interroger les utilisateurs et coordinateurs locaux d'un ou plusieurs territoires donnés. Cela pourrait guider avantageusement les développements futurs ainsi que les potentielles aides publiques, qu'elles soient en euros ou en ğ1.

Par ailleurs, des applications bien implantées au niveau local telles que Gecko G1nkgo et Cesium, pourraient permettre d'échanger de l'information au plus près du public visé. Cela pourrait passer par une page d'actualités directement insérée dans l'application ainsi que des questionnaires interactifs permettant de collecter des retours qualitatifs et quantitatifs.

Cela ouvre la perspective d'un observatoire de proximité sur le terrain, pour étudier non pas des comportements individuels, mais des comportements collectifs. A la fois dans un registre local d'un bassin de vie qui fait société autour de l'échange et à l'échelle plus élargie d'une agrégation autour d'une commune mesure, une monnaie libre.

Enfin, déjà évoquée, la toile de confiance, en tant que système d'authentification intègre, décentralisé et non biométrique, ouvre des perspectives considérables. Non seulement en termes fonctionnels pour toutes les relations contractuelles entre plusieurs personnes, mais aussi sur la capacité de délibération et de décision populaire, pour manipuler les nuances, la complexité, l'évolutivité des positions de chacun.e.
