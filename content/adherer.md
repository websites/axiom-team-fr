---
title: Adhérer
description: "Adhésion et vie Association "
---

L’adhésion constitue une ressource régulière qui permet de financer le fonctionnement de l’association elle-même, quelques aspects techniques lorsqu’ils sont nécessaires, et surtout les **rassemblements humains**, comme les « axiom boat », les hackathons, et autres événements qu’elle organisera.

Votre adhésion a également une grande valeur symbolique car elle manifeste votre soutien à notre démarche. Nous aimerions accueillir parmi les adhérents une grande majorité des membres de la monnaie libre.

Votre adhésion vous donne une voix aux assemblées générales ordinaires et extraordinaires auxquelles vous serez invités.

## Montant de l’adhésion

Il est double :

- 12 DUĞ1 pour l'année afin de valoriser en monnaie libre tout ce qui est accepté par les « monnaie-libristes ».
- 12 € pour l'année (1 € / mois) afin de pourvoir à tout ce que l’on ne peut pas encore régler en monnaie libre.
  Il n'y a aucune corrélation entre les 2 montants, ce sont deux choses totalement différentes.
- Clé publique pour la partie Ğ1 : 8CWuf4f1jYoVzHh4DEFpCyzZYC1pgz4t2wU8F2zKCthh 
  RQ : merci de mettre en commentaire de transaction : adhesion Axiom 2024
- Plateforme **[Hello Asso](https://www.helloasso.com/associations/axiom-team/adhesions/adhesion-2024-axiom-team)** pour la partie euros.
  Nous avons choisi celle-ci car elle est très souple à l'usage et n'impose pas une contribution à la plateforme elle-même, ni à vous ni à nous.

Si vous préférez adhérer autrement que par cette plateforme, vous pouvez opter pour une cotisation annuelle de 12€ par chèque à l'ordre de : “Axiom Team” et l'envoyer à l'adresse suivante :
AXIOM TEAM
Chez Mr Nguyen Minh
14 rue Johannes Kepler
31500 Toulouse.

Vous pouvez également procéder à un virement bancaire IBAN : FR76 1027 8022 0000 0209 3000 182

Si vous souhaitez soutenir ponctuellement l’association sans devenir adhérent, vous pouvez également faire un don à cet [endroit](https://www.helloasso.com/associations/axiom-team/).
Découvrez également nos campagnes de "dons fléchés" (au même endroit ;-), qui vous permettent d'orienter votre don sur un des usages définis par les cercles de répartition des devs.

## Statuts de l'association

Voici le lien sur les [statuts originaux](https://cloud.axiom-team.fr/s/Dwzpw7PAofNroof) (en cours de ré-écriture).

Notre dernière Assemblée Générale a eu lieu le 26  février 2023 au siège de l'association. La prochaine a lieu le 3 février 2024 dans la Drôme.

Vous trouverez les bilans financiers sur le [Forum Monnaie Libre ](https://forum.monnaie-libre.fr/t/compta-axiom-team-bilans-et-comptes-de-resultats/26649) ainsi que la composition du [Conseil d'Administration 2023](https://forum.monnaie-libre.fr/t/membres-bureau-et-ca-axiom-team-2023/26650)

Nous travaillons encore à la mise à jour de nos statuts et Règlement Intérieur, notamment pour les rendre davantage conformes aux pré-requis de l'agrément ESUS.
