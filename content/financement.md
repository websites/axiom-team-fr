---
title: Financement
description: Soutenir le développement, c'est pour une grande part, trouver des
  financements afin de rétribuer des développeurs à la demande.
---

Axiom Team soutient les développeurs qui le souhaitent en mettant en œuvre et en maintenant les **outils et environnements techniques** qui leur permettent de coder ensemble. Elle cherche également à multiplier les rencontres physiques car ces moments sont cruciaux et déterminants pour maintenir et développer la June.
Une action forte et essentielle pour axiom est de parvenir à constituer un fond de réserve qui permette de **rétribuer les développeurs** qui en ont besoin. Ces besoins sont variables et aléatoires, ils répondent à des situations et des contextes très différents.
Ce fond n'est donc pas tant à considérer comme une enveloppe budgétaire annuelle, mais plutôt comme un flux, avec un débit à atteindre, ajustable.

## Première source sacrée

### Vous

La première source, la plus importante du point de vue de sa fiabilité et du point de vue symbolique, ce sont les dons que vous faites.

Fin d'année 2022, nous avons mené une campagne pendant 2 mois intenses. Vous avez donné 35.000 € à cette occasion, ce qui fut remarquable mais c'est loin de suffire. C'est la raison pour laquelle nous lançons maintenant des campagnes permanentes de dons fléchés. Nous prévoyons une comm plus douce, plus pédagogique.

Elles seront complétées par des opérations ponctuelles avec un objectif précis, les campagnes de crowdfunding, et une comm plus événementielle.

Pour comprendre le paysage, assez simple, nous devons schématiquement :

- Donner une visibilité et une trésorerie semestrielle pour permettre l'engagement sur un CDI, au moins sur une année sinon deux. Plus si affinités, mais de demain nul n'est certain.
- Donner des enveloppes à tirage pour les développeurs qui souhaitent facturer certaines heures passées.
- Pouvoir faire appel à des prestations ponctuelles sur commande pour des tâches ou des expertises spécifiques.
- Permettre d'autres temps partiels et des stages pour compléter et mieux répartir le vaste chantier de la migration.

Nous vous invitons donc à **financer cette aventure de la june** pour qu'elle vive dans la longue durée, **sous forme de dons**. Plus vous pratiquerez des dons mensuels, plus nous aurons de débit. Plus vous faites des dons ponctuels, plus nous aurons de visibilité dans le temps.

\[ bouton [Hello Asso](https://www.helloasso.com/associations/axiom-team) ]

### Fléchage des dons

Vous pouvez maintenant **flécher vos dons**, en précisant votre intention pour orienter chaque dons sur un des usages définis par le premier cercle de répartition.

Ce sont bien les développeurs qui font les arbitrages. Axiom met en œuvre la robinetterie mais ce sont les cercles qui font les réglages. Puis ce sont les devs destinataires eux-mêmes qui ouvrent et ferment les robinets en co-responsabilité.

Aujourd'hui il y a donc 6 options de fléchage :

- Développements sur l'écosystème Duniter, donc à la fois sur V1 et V2s. Dit le "back-end".
- Développement des applications et utilitaires que l'on voit, comme Cesium de kimamila.
- Réserve pour auto-financements et/ou trésorerie requis par les subventions.
- Provision pour salarier une personne dédiée à la recherche et candidatures aux subventions.
- Production de supports de comm.

Les 4 premiers fléchages sont directement arbitrés par les cercles de devs, le 5ème fléchage est arbitré par le cercle des adhérents axiom.

Vous pouvez également préférer un don libre, auquel cas le cercle des adhérents axiom fera l'arbitrage pour rediriger sur un des 5 postes en fonction des priorités du moment.

\[ bouton Hello Asso ]

#### Quelle valeur en euros pour la "semaine dev" ?

Il serait illusoire de fixer des objectifs annuels, mais nous pouvons convenir d'un **indicateur commun**, accessible à tous, pour savoir où nous en sommes.

En gros nous oscillons entre une prime de stage +, un smic +, une facturation à 500€ la journée, une "valeur de marché prestataires" à 800 € jour, une référence de salaire à 120k€, etc... Une sorte de **barycentre pondéré** par notre contexte réel (personnes connues avec des demandes positionnées), et faisant l'objet d'une attraction irrésistible vers l'arrondi, donne à ce jour 1.000€ la semaine dev.
Nous pourrons - évidemment - ajuster cet indicateur et procéder à un calcul plus fin et proche de la réalité, mais lorsque nous aurons vécu cette réalité.

On peut imaginer des catégories pour refléter les écarts entre un stage et une commande facturée, mais pour la comm et notamment pour démarrer, miser sur la simplicité semble sage.

On peut poser l'unité suivante : la "**semaine dev à 1.000€**", _(ou **smendev** ;-)_.

Et nous pouvons utiliser, pour qualifier le fond à constituer, l'image du **château d'eau**. Nous aurons donc une capacité en "semaines dev" et un débit corrélé, autrement une capacité dans la durée et une capacité en nombre de devs simultanés, pour un run par exemple.

---



NOTA  :

Axiom-team a choisi de ne pas développer d'activité générant des revenus en euros.

L'association trouve très cohérent de rassembler des euros pour rétribuer les développeurs qui se mobilisent dans la durée, de financer ce développement de logiciels libres avec des UNL, la plupart des logiciels libres le sont. Mais pas de développer une économie monnaie-libriste avec une monnaie non libre.

C'est pourquoi Axiom-Team collecte également les DUs, qui sont très bienvenus, car l'association cherchera toujours à jouer son propre petit rôle dans l'émergence d'une économie libriste. Par exemple en valorisant les personnes non développeuses mais également très mobilisées sur les missions de l'asso.

## Seconde source : les subventions

L'association ne peut donc pas recourir aux financements dits de fonds propres, qui soit dit en passant peuvent fréquemment coûter 6% dans le milieu des scop et de l'ESS. L'association ne peut donc avoir recours qu'à des subventions. Or elles sont plus rares, et souvent territorialisées (régions, départements, agglos et intercos, mairies). L'Ademe est une rare exception à la fois très indépendante, dotée du droit de subventionner et non locale (disons nationale).

Nous allons souscrire à d'autres subventions. Mais cela dépend des personnes qui se mobilisent dans la durée pour trouver des subventions, rédiger les dossiers, les représenter et les suivre.&#x20;

Cette source est déterminante pour la visibilité sur les mois à venir, la capacité du réservoir. Elle ne peut pas  garantir le flux. Il n'y a que vos dons pour cela.

Nous adapterons les débits de distribution en fonction des événements et des besoins, naturellement, mais il faut une première capacité pour pouvoir ouvrir certains robinets, comme avoir un premier niveau de pression.

Par exemple, une personne qui se positionne sur un CDI a besoin de 46 "smendev" dans le réservoir. Une personne qui se positionne sur de la facture à l'avenant, peut avoir besoin de 12 "smendev" dans le pipe pour qu'elle estime que ça vaille le coup. Etc...
