import { defineNuxtPlugin } from '#app'

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.hook('mdc-editor:config', ({ config, editor }: { config: any, editor: any }) => {
    config.components.push({
      displayName: 'Liste de pages',
      description: 'Insert une liste de pages',
      group: 'Blocs avancés',
      icon: 'i-lucide-layout-list',
      handleMenu: true,
      slashMenu: true,
      onSelect: () => {
        editor.commands.insertContainerComponent({
          name: 'liste',
        })
      },
    })

    config.components.push({
      displayName: "Grille d'images",
      description: "Insert une grille d'images (masonry)",
      group: 'Blocs avancés',
      icon: 'i-lucide-layout-grid',
      handleMenu: true,
      slashMenu: true,
      onSelect: () => {
        editor.commands.insertContainerComponent({
          name: 'grid',
        })
      },
    })

    return config
  })
})
